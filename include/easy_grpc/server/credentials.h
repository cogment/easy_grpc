// Copyright 2019 Age of Minds inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef EASY_GRPC_SERVER_CREDENTIALS_H_INCLUDED
#define EASY_GRPC_SERVER_CREDENTIALS_H_INCLUDED

#include "grpc/grpc_security.h"

namespace easy_grpc {
namespace server {

class Credentials {
 public:
  Credentials();
  Credentials(const char* root_certs, const char* private_key, const char* trust_chain,
              grpc_ssl_client_certificate_request_type client_requirements =
                  grpc_ssl_client_certificate_request_type::GRPC_SSL_REQUEST_AND_REQUIRE_CLIENT_CERTIFICATE_AND_VERIFY);
  ~Credentials();

  Credentials(Credentials&&);
  Credentials& operator=(Credentials&&);
  Credentials(const Credentials&) = delete;
  Credentials& operator=(const Credentials&) = delete;

  grpc_server_credentials* get_credentials() const { return creds_; }

 private:
  grpc_server_credentials* creds_;
};

}  // namespace server
}  // namespace easy_grpc

#endif