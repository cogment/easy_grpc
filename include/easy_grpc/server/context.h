// Copyright 2020 AI Redefined inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef EASY_GRPC_SERVER_CONTEXT_H_INCLUDED
#define EASY_GRPC_SERVER_CONTEXT_H_INCLUDED

#include "easy_grpc/server/methods/call_handler.h"

namespace easy_grpc {

// Technically, this could be templated on the specific Call_handler subclass
// to use.
// If there are ever method-specific functionalities to add to the Context,
// then we can change it then.
class Context {
  using handler_t = easy_grpc::server::detail::Call_handler;

  handler_t& call_;

 public:
  // Constructor intentionally implicit.
  Context(handler_t& call) : call_(call) {}

  // Looks up a metadata field. Will throw std::out_of_bounds if the field is
  // missing.
  std::string_view get_client_header(std::string_view key);

  // Since we are dealing with weak references, it's worth being explicit that
  // the default behavior is indeed desirable here for the rule of 5.
  Context(const Context&) = default;
  Context(Context&&) = default;
  Context& operator=(const Context&) = default;
  Context& operator=(Context&&) = default;
  ~Context() = default;
};
}  // namespace easy_grpc

#endif