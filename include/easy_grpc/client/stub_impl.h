// Copyright 2019 Age of Minds inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef EASY_GRPC_CLIENT_STUB_IMPL_INCLUDED_H
#define EASY_GRPC_CLIENT_STUB_IMPL_INCLUDED_H

#include "easy_grpc/client/channel.h"
#include "easy_grpc/completion_queue.h"
#include "easy_grpc/error.h"
#include "easy_grpc/serialize.h"

#include "grpc/grpc.h"
#include "grpc/support/alloc.h"

#include <cstring>
#include <iostream>
#include <queue>

namespace easy_grpc {

namespace client {
struct Call_options {
  Completion_queue* completion_queue = nullptr;
  gpr_timespec deadline = gpr_inf_future(GPR_CLOCK_REALTIME);
  std::vector<grpc_metadata>* headers = nullptr;
};

//*********************************************************************************//
namespace detail {
template <typename RepT>
class Unary_call_completion final : public Completion_callback {
 public:
  Unary_call_completion(grpc_call* call) : call_(call) {
    grpc_metadata_array_init(&trailing_metadata_);
    grpc_metadata_array_init(&server_metadata_);
    status_details_ = grpc_empty_slice();
  }

  ~Unary_call_completion() {
    grpc_metadata_array_destroy(&server_metadata_);
    grpc_metadata_array_destroy(&trailing_metadata_);
    grpc_slice_unref(status_details_);
    gpr_free(const_cast<char*>(error_string_));

    grpc_byte_buffer_destroy(recv_buffer_);

    grpc_call_unref(call_);
  }

  void fail() {
    try {
      throw error::internal("failed to start call");
    } catch (...) {
      rep_.set_exception(std::current_exception());
    }
  }

  bool exec(bool, std::bitset<4>) noexcept override {
    if (status_ == GRPC_STATUS_OK) {
      rep_.set_value(deserialize<RepT>(recv_buffer_));
    } else {
      rep_.set_exception(std::make_exception_ptr(Rpc_error(status_, error_string_)));
    }

    gpr_free(const_cast<char*>(error_string_));
    error_string_ = nullptr;

    return true;
  }

  grpc_call* call_;
  grpc_metadata_array server_metadata_;
  Promise<RepT> rep_;
  grpc_byte_buffer* recv_buffer_ = nullptr;

  grpc_metadata_array trailing_metadata_;
  grpc_status_code status_ = GRPC_STATUS_UNKNOWN;
  grpc_slice status_details_;
  const char* error_string_ = nullptr;
};
}  // namespace detail

template <typename RepT, typename ReqT>
Future<RepT> start_unary_call(Channel* channel, void* tag, const ReqT& req, Call_options options) {
  assert(options.completion_queue);

  auto call = grpc_channel_create_registered_call(channel->handle(), nullptr, GRPC_PROPAGATE_DEFAULTS,
                                                  options.completion_queue->handle(), tag, options.deadline, nullptr);
  auto completion = new detail::Unary_call_completion<RepT>(call);
  auto buffer = serialize(req);

  std::array<grpc_op, 6> ops;

  ops[0].op = GRPC_OP_SEND_INITIAL_METADATA;
  ops[0].flags = 0;
  ops[0].reserved = nullptr;
  ops[0].data.send_initial_metadata.count = 0;
  ops[0].data.send_initial_metadata.maybe_compression_level.is_set = 0;

  if (options.headers != nullptr && options.headers->size() > 0) {
    ops[0].data.send_initial_metadata.count = options.headers->size();
    ops[0].data.send_initial_metadata.metadata = options.headers->data();
  }

  ops[1].op = GRPC_OP_SEND_MESSAGE;
  ops[1].flags = 0;
  ops[1].reserved = nullptr;
  ops[1].data.send_message.send_message = buffer;

  ops[2].op = GRPC_OP_RECV_INITIAL_METADATA;
  ops[2].flags = 0;
  ops[2].reserved = 0;
  ops[2].data.recv_initial_metadata.recv_initial_metadata = &completion->server_metadata_;

  ops[3].op = GRPC_OP_RECV_MESSAGE;
  ops[3].flags = 0;
  ops[3].reserved = 0;
  ops[3].data.recv_message.recv_message = &completion->recv_buffer_;

  ops[4].op = GRPC_OP_SEND_CLOSE_FROM_CLIENT;
  ops[4].flags = 0;
  ops[4].reserved = 0;

  ops[5].op = GRPC_OP_RECV_STATUS_ON_CLIENT;
  ops[5].flags = 0;
  ops[5].reserved = 0;
  ops[5].data.recv_status_on_client.trailing_metadata = &completion->trailing_metadata_;
  ops[5].data.recv_status_on_client.status = &completion->status_;
  ops[5].data.recv_status_on_client.status_details = &completion->status_details_;
  ops[5].data.recv_status_on_client.error_string = &completion->error_string_;

  auto result = completion->rep_.get_future();
  auto status = grpc_call_start_batch(call, ops.data(), ops.size(), completion->completion_tag().data, nullptr);

  if (status != GRPC_CALL_OK) {
    completion->fail();
    delete completion;
  }

  grpc_byte_buffer_destroy(buffer);

  return result;
}

//*********************************************************************************//

namespace detail {
template <typename RepT>
class Streaming_call_session final : public Completion_callback {
 public:
  Streaming_call_session(grpc_call* call) : call_(call) {
    grpc_metadata_array_init(&trailing_metadata_);
    grpc_metadata_array_init(&server_metadata_);
    status_details_ = grpc_empty_slice();
  }

  ~Streaming_call_session() {
    grpc_metadata_array_destroy(&server_metadata_);
    grpc_metadata_array_destroy(&trailing_metadata_);
    grpc_slice_unref(status_details_);
    gpr_free(const_cast<char*>(error_string_));

    grpc_byte_buffer_destroy(recv_buffer_);

    grpc_call_unref(call_);
  }

  bool exec(bool, std::bitset<4>) noexcept override {
    const bool all_done = finishing_;

    if (!all_done) {
      if (recv_buffer_ != nullptr) {
        auto data = recv_buffer_;
        recv_buffer_ = nullptr;

        std::array<grpc_op, 1> ops;

        ops[0].op = GRPC_OP_RECV_MESSAGE;
        ops[0].flags = 0;
        ops[0].reserved = 0;
        ops[0].data.recv_message.recv_message = &recv_buffer_;

        __attribute__((__unused__)) auto call_status =
            grpc_call_start_batch(call_, ops.data(), ops.size(), completion_tag().data, nullptr);
        assert(call_status == GRPC_CALL_OK);

        reply_stream_promise_.push(deserialize<RepT>(data));
        grpc_byte_buffer_destroy(data);

      } else {
        gpr_free(const_cast<char*>(error_string_));
        error_string_ = nullptr;

        std::array<grpc_op, 2> ops;

        ops[0].op = GRPC_OP_SEND_CLOSE_FROM_CLIENT;
        ops[0].flags = 0;
        ops[0].reserved = 0;

        ops[1].op = GRPC_OP_RECV_STATUS_ON_CLIENT;
        ops[1].flags = 0;
        ops[1].reserved = 0;
        ops[1].data.recv_status_on_client.trailing_metadata = &trailing_metadata_;
        ops[1].data.recv_status_on_client.status = &status_;
        ops[1].data.recv_status_on_client.status_details = &status_details_;
        ops[1].data.recv_status_on_client.error_string = &error_string_;

        finishing_ = true;

        __attribute__((__unused__)) auto call_status =
            grpc_call_start_batch(call_, ops.data(), ops.size(), completion_tag().data, nullptr);
        assert(call_status == GRPC_CALL_OK);
      }
    } else {
      if (status_ == GRPC_STATUS_OK) {
        reply_stream_promise_.complete();
      } else {
        reply_stream_promise_.set_exception(std::make_exception_ptr(Rpc_error(status_, error_string_)));
      }

      gpr_free(const_cast<char*>(error_string_));
      error_string_ = nullptr;
    }

    return all_done;
  }

  grpc_call* call_;
  Stream_promise<RepT> reply_stream_promise_;
  grpc_byte_buffer* recv_buffer_ = nullptr;

  bool finishing_ = false;
  grpc_metadata_array server_metadata_;
  grpc_metadata_array trailing_metadata_;
  grpc_status_code status_ = GRPC_STATUS_UNKNOWN;
  grpc_slice status_details_;
  const char* error_string_ = nullptr;
};
}  // namespace detail

template <typename RepT, typename ReqT>
Stream_future<RepT> start_server_streaming_call(Channel* channel, void* tag, const ReqT& req, Call_options options) {
  assert(options.completion_queue);

  auto call = grpc_channel_create_registered_call(channel->handle(), nullptr, GRPC_PROPAGATE_DEFAULTS,
                                                  options.completion_queue->handle(), tag, options.deadline, nullptr);
  auto completion = new detail::Streaming_call_session<RepT>(call);
  auto send_buffer = serialize(req);

  std::array<grpc_op, 4> ops;

  ops[0].op = GRPC_OP_SEND_INITIAL_METADATA;
  ops[0].flags = 0;
  ops[0].reserved = nullptr;
  ops[0].data.send_initial_metadata.count = 0;
  ops[0].data.send_initial_metadata.maybe_compression_level.is_set = 0;

  if (options.headers != nullptr && options.headers->size() > 0) {
    ops[0].data.send_initial_metadata.count = options.headers->size();
    ops[0].data.send_initial_metadata.metadata = options.headers->data();
  }

  ops[1].op = GRPC_OP_SEND_MESSAGE;
  ops[1].flags = 0;
  ops[1].reserved = nullptr;
  ops[1].data.send_message.send_message = send_buffer;

  ops[2].op = GRPC_OP_RECV_INITIAL_METADATA;
  ops[2].flags = 0;
  ops[2].reserved = 0;
  ops[2].data.recv_initial_metadata.recv_initial_metadata = &completion->server_metadata_;

  ops[3].op = GRPC_OP_RECV_MESSAGE;
  ops[3].flags = 0;
  ops[3].reserved = 0;
  ops[3].data.recv_message.recv_message = &completion->recv_buffer_;

  auto status =
      grpc_call_start_batch(completion->call_, ops.data(), ops.size(), completion->completion_tag().data, nullptr);

  if (status != GRPC_CALL_OK) {
    completion->reply_stream_promise_.set_exception(std::make_exception_ptr(error::internal("failed to start call")));
  }

  grpc_byte_buffer_destroy(send_buffer);

  return completion->reply_stream_promise_.get_future();
}

//*********************************************************************************//

// This is not quite as nice as it could be...
//
template <typename RepT, typename ReqT>
class Client_streaming_call_session final : public Completion_callback {
 public:
  Client_streaming_call_session(grpc_call* call, Call_options options) : call_(call) {
    grpc_metadata_array_init(&trailing_metadata_);
    grpc_metadata_array_init(&server_metadata_);
    status_details_ = grpc_empty_slice();

    std::array<grpc_op, 2> pending_ops;

    pending_ops[0].op = GRPC_OP_SEND_INITIAL_METADATA;
    pending_ops[0].flags = 0;
    pending_ops[0].reserved = nullptr;
    pending_ops[0].data.send_initial_metadata.count = 0;
    pending_ops[0].data.send_initial_metadata.maybe_compression_level.is_set = 0;

    if (options.headers != nullptr && options.headers->size() > 0) {
      pending_ops[0].data.send_initial_metadata.count = options.headers->size();
      pending_ops[0].data.send_initial_metadata.metadata = options.headers->data();
    }

    pending_ops[1].op = GRPC_OP_RECV_INITIAL_METADATA;
    pending_ops[1].flags = 0;
    pending_ops[1].reserved = 0;
    pending_ops[1].data.recv_initial_metadata.recv_initial_metadata = &server_metadata_;

    grpc_call_start_batch(call_, pending_ops.data(), pending_ops.size(), completion_tag().data, nullptr);
    batch_in_flight_ = true;

    req_.get_future()
        .for_each([this](ReqT req) {
          std::lock_guard l(mtx_);

          auto buffer = serialize(req);

          grpc_op op;
          op.op = GRPC_OP_SEND_MESSAGE;
          op.flags = 0;
          op.reserved = nullptr;
          op.data.send_message.send_message = buffer;

          pending_ops_.push(op);

          // Send whatever we have.
          flush_();
        })
        .finally([this](expected<void>) {
          std::lock_guard l(mtx_);
          finished_ = true;

          if (!batch_in_flight_) {
            send_end();
          }
        });
  }

  ~Client_streaming_call_session() {
    assert(pending_ops_.empty());

    grpc_metadata_array_destroy(&server_metadata_);
    grpc_metadata_array_destroy(&trailing_metadata_);
    grpc_slice_unref(status_details_);
    gpr_free(const_cast<char*>(error_string_));

    grpc_byte_buffer_destroy(recv_buffer_);

    grpc_call_unref(call_);
  }

  void flush_() {
    if (batch_in_flight_) {
      return;
    }

    batch_in_flight_ = true;

    auto op = pending_ops_.front();
    pending_ops_.pop();

    assert(op.op == GRPC_OP_SEND_MESSAGE);

    auto status = grpc_call_start_batch(call_, &op, 1, completion_tag().data, nullptr);

    if (status != GRPC_CALL_OK) {
      std::cerr << grpc_call_error_to_string(status) << "\n";
    }
    assert(status == GRPC_CALL_OK);

    grpc_byte_buffer_destroy(op.data.send_message.send_message);
  }

  void send_end() {
    std::array<grpc_op, 3> ops;

    ops[0].op = GRPC_OP_SEND_CLOSE_FROM_CLIENT;
    ops[0].flags = 0;
    ops[0].reserved = 0;

    ops[1].op = GRPC_OP_RECV_MESSAGE;
    ops[1].flags = 0;
    ops[1].reserved = 0;
    ops[1].data.recv_message.recv_message = &recv_buffer_;

    ops[2].op = GRPC_OP_RECV_STATUS_ON_CLIENT;
    ops[2].flags = 0;
    ops[2].reserved = 0;
    ops[2].data.recv_status_on_client.trailing_metadata = &trailing_metadata_;
    ops[2].data.recv_status_on_client.status = &status_;
    ops[2].data.recv_status_on_client.status_details = &status_details_;
    ops[2].data.recv_status_on_client.error_string = &error_string_;

    auto status = grpc_call_start_batch(call_, ops.data(), ops.size(), completion_tag().data, nullptr);

    if (status != GRPC_CALL_OK) {
      std::cerr << grpc_call_error_to_string(status) << "\n";
    }
    assert(status == GRPC_CALL_OK);

    end_sent_ = true;
  }

  bool exec(bool, std::bitset<4>) noexcept override {
    std::lock_guard l(mtx_);

    batch_in_flight_ = false;

    if (!pending_ops_.empty()) {
      flush_();
      return false;
    }

    if (finished_) {
      if (end_sent_) {
        if (status_ == GRPC_STATUS_OK) {
          rep_.set_value(deserialize<RepT>(recv_buffer_));
        } else {
          rep_.set_exception(std::make_exception_ptr(Rpc_error(status_, error_string_)));
        }

        gpr_free(const_cast<char*>(error_string_));
        error_string_ = nullptr;

        return true;
      }
      send_end();
    }

    gpr_free(const_cast<char*>(error_string_));
    error_string_ = nullptr;

    return false;
  }

  // I really wish we could do this without a mutex, somehow...
  std::mutex mtx_;
  bool batch_in_flight_ = false;
  bool finished_ = false;
  bool end_sent_ = false;

  grpc_call* call_;
  grpc_metadata_array server_metadata_;

  Stream_promise<ReqT> req_;
  Promise<RepT> rep_;

  grpc_byte_buffer* recv_buffer_ = nullptr;

  grpc_metadata_array trailing_metadata_;
  grpc_status_code status_ = GRPC_STATUS_UNKNOWN;
  grpc_slice status_details_;
  const char* error_string_ = nullptr;

  std::queue<grpc_op> pending_ops_;
};

template <typename RepT, typename ReqT>
std::tuple<Stream_promise<ReqT>, Future<RepT>> start_client_streaming_call(Channel* channel, void* tag,
                                                                           Call_options options) {
  assert(options.completion_queue);

  auto call = grpc_channel_create_registered_call(channel->handle(), nullptr, GRPC_PROPAGATE_DEFAULTS,
                                                  options.completion_queue->handle(), tag, options.deadline, nullptr);

  auto call_session = new Client_streaming_call_session<RepT, ReqT>(call, options);

  return {std::move(call_session->req_), call_session->rep_.get_future()};
}

//*********************************************************************************//

template <typename RepT, typename ReqT>
class Bidir_streaming_call_session final : public Completion_callback {
 public:
  Bidir_streaming_call_session(grpc_call* call, Stream_future<ReqT> req_stream, Call_options options) : call_(call) {
    grpc_metadata_array_init(&trailing_metadata_);
    grpc_metadata_array_init(&server_metadata_);
    status_details_ = grpc_empty_slice();

    // Launch the metadata exchange
    std::array<grpc_op, 1> pending_ops;

    pending_ops[0].op = GRPC_OP_SEND_INITIAL_METADATA;
    pending_ops[0].flags = 0;
    pending_ops[0].reserved = nullptr;
    pending_ops[0].data.send_initial_metadata.count = 0;
    pending_ops[0].data.send_initial_metadata.maybe_compression_level.is_set = 0;

    if (options.headers != nullptr && options.headers->size() > 0) {
      pending_ops[0].data.send_initial_metadata.count = options.headers->size();
      pending_ops[0].data.send_initial_metadata.metadata = options.headers->data();
    }

    can_send_ = false;
    static constexpr uint64_t HANDSHAKE = 0x02;
    grpc_call_start_batch(call_, pending_ops.data(), pending_ops.size(), completion_tag(HANDSHAKE).data, nullptr);

    // Get ready to send requests
    req_stream
        .for_each([this](ReqT req) {
          std::lock_guard l(mtx_);

          auto buffer = serialize(req);

          grpc_op op;
          op.op = GRPC_OP_SEND_MESSAGE;
          op.flags = 0;
          op.reserved = nullptr;
          op.data.send_message.send_message = buffer;

          pending_ops_.push(op);

          // Send whatever we have.
          flush_();
        })
        .finally([this](expected<void> s) {
          std::lock_guard l(mtx_);
          finished_sending_ = true;

          try {
            s.value();
            if (can_send_) {
              send_client_end();
            }
          } catch (const std::exception& e) {
            std::cerr << "easygrpc bidir stream end error: " << e.what() << "\n";
            grpc_call_cancel_with_status(call_, GRPC_STATUS_INTERNAL, e.what(), nullptr);
          }
        });
  }

  ~Bidir_streaming_call_session() {
    assert(pending_ops_.empty());

    grpc_metadata_array_destroy(&server_metadata_);
    grpc_metadata_array_destroy(&trailing_metadata_);
    grpc_slice_unref(status_details_);
    gpr_free(const_cast<char*>(error_string_));

    grpc_byte_buffer_destroy(recv_buffer_);

    grpc_call_unref(call_);
  }

  void flush_() {
    if (!can_send_) {
      return;
    }

    can_send_ = false;

    auto op = pending_ops_.front();
    pending_ops_.pop();

    assert(op.op == GRPC_OP_SEND_MESSAGE);

    static constexpr uint64_t WRITE_OP = 0x01;
    auto status = grpc_call_start_batch(call_, &op, 1, completion_tag(WRITE_OP).data, nullptr);

    if (status != GRPC_CALL_OK) {
      std::cerr << grpc_call_error_to_string(status) << "\n";
      assert(false);
    }

    grpc_byte_buffer_destroy(op.data.send_message.send_message);
  }

  void send_client_end() {
    std::array<grpc_op, 1> ops;

    ops[0].op = GRPC_OP_SEND_CLOSE_FROM_CLIENT;
    ops[0].flags = 0;
    ops[0].reserved = 0;

    static constexpr uint64_t ENDING = 0x04;
    auto status = grpc_call_start_batch(call_, ops.data(), ops.size(), completion_tag(ENDING).data, nullptr);

    if (status != GRPC_CALL_OK) {
      std::cerr << grpc_call_error_to_string(status) << "\n";
    }
    assert(status == GRPC_CALL_OK);

    client_end_sent_ = true;
  }

  void close() {
    std::array<grpc_op, 1> ops;

    ops[0].op = GRPC_OP_RECV_STATUS_ON_CLIENT;
    ops[0].flags = 0;
    ops[0].reserved = 0;
    ops[0].data.recv_status_on_client.trailing_metadata = &trailing_metadata_;
    ops[0].data.recv_status_on_client.status = &status_;
    ops[0].data.recv_status_on_client.status_details = &status_details_;
    ops[0].data.recv_status_on_client.error_string = &error_string_;

    static constexpr uint64_t CLOSING = 0x08;
    auto status = grpc_call_start_batch(call_, ops.data(), ops.size(), completion_tag(CLOSING).data, nullptr);

    if (status != GRPC_CALL_OK) {
      std::cerr << grpc_call_error_to_string(status) << "\n";
    }
    assert(status == GRPC_CALL_OK);

    recv_status_sent_ = true;
  }

  bool exec(bool, std::bitset<4> flags) noexcept override {
    bool write_op = flags.test(0);
    bool handshake = flags.test(1);
    bool ending = flags.test(2);
    bool closing = flags.test(3);

    if (closing) {
      if (status_ == GRPC_STATUS_OK) {
        rep_.complete();
      } else {
        rep_.set_exception(std::make_exception_ptr(Rpc_error(status_, error_string_)));
      }

      gpr_free(const_cast<char*>(error_string_));
      error_string_ = nullptr;

      return true;
    }

    gpr_free(const_cast<char*>(error_string_));
    error_string_ = nullptr;

    std::unique_lock l(mtx_);

    if (handshake) {
      // Allow the sending of messages
      can_send_ = true;
      if (!pending_ops_.empty()) {
        flush_();
      }

      // Start receiving messages from the server
      std::array<grpc_op, 2> ops;
      ops[0].op = GRPC_OP_RECV_MESSAGE;
      ops[0].flags = 0;
      ops[0].reserved = 0;
      ops[0].data.recv_message.recv_message = &recv_buffer_;

      // "standard" grpc servers send metadata alongside the first message...
      ops[1].op = GRPC_OP_RECV_INITIAL_METADATA;
      ops[1].flags = 0;
      ops[1].reserved = 0;
      ops[1].data.recv_initial_metadata.recv_initial_metadata = &server_metadata_;

      __attribute__((__unused__)) auto call_status =
          grpc_call_start_batch(call_, ops.data(), ops.size(), completion_tag().data, nullptr);
      assert(call_status == GRPC_CALL_OK);

      l.unlock();
    } else if (ending) {
      client_end_complete_ = true;
      if (finished_receiving_) {
        close();
      }
    } else if (write_op) {
      // That was the end of a write op
      can_send_ = true;

      if (!pending_ops_.empty()) {
        flush_();
      } else if (finished_sending_) {
        send_client_end();
      }
    } else {
      // That was the end of a read op
      if (recv_buffer_ != nullptr) {
        auto data = recv_buffer_;
        recv_buffer_ = nullptr;

        std::array<grpc_op, 1> ops;

        ops[0].op = GRPC_OP_RECV_MESSAGE;
        ops[0].flags = 0;
        ops[0].reserved = 0;
        ops[0].data.recv_message.recv_message = &recv_buffer_;

        __attribute__((__unused__)) auto call_status =
            grpc_call_start_batch(call_, ops.data(), ops.size(), completion_tag().data, nullptr);
        assert(call_status == GRPC_CALL_OK);

        auto msg = deserialize<RepT>(data);
        grpc_byte_buffer_destroy(data);
        l.unlock();

        rep_.push(std::move(msg));
      } else {
        finished_receiving_ = true;

        if (client_end_complete_) {
          close();
        }
      }
    }

    return false;
  }

  Stream_promise<RepT> rep_;

  // I really wish we could do this without a mutex, somehow...
  std::mutex mtx_;
  bool can_send_ = false;
  bool finished_sending_ = false;
  bool finished_receiving_ = false;

  bool client_end_sent_ = false;
  bool client_end_complete_ = false;
  bool recv_status_sent_ = false;

  grpc_call* call_;
  grpc_metadata_array server_metadata_;
  grpc_byte_buffer* recv_buffer_ = nullptr;

  grpc_metadata_array trailing_metadata_;
  grpc_status_code status_ = GRPC_STATUS_UNKNOWN;
  grpc_slice status_details_;
  const char* error_string_ = nullptr;

  std::queue<grpc_op> pending_ops_;
};

template <typename RepT, typename ReqT>
std::tuple<Stream_promise<ReqT>, Stream_future<RepT>> start_bidir_streaming_call(Channel* channel, void* tag,
                                                                                 Call_options options) {
  assert(options.completion_queue);

  auto call = grpc_channel_create_registered_call(channel->handle(), nullptr, GRPC_PROPAGATE_DEFAULTS,
                                                  options.completion_queue->handle(), tag, options.deadline, nullptr);

  Stream_promise<ReqT> req;
  auto call_session = new Bidir_streaming_call_session<RepT, ReqT>(call, req.get_future(), options);

  return {std::move(req), call_session->rep_.get_future()};
}

}  // namespace client

}  // namespace easy_grpc
#endif
