// Copyright 2019 Age of Minds inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef EASY_GRPC_CLIENT_CREDENTIALS_H_INCLUDED
#define EASY_GRPC_CLIENT_CREDENTIALS_H_INCLUDED

#include "grpc/grpc_security.h"

namespace easy_grpc {
namespace client {

class Credentials {
 public:
  Credentials();
  Credentials(const char* root_certs, const char* private_key, const char* trust_chain);
  ~Credentials();

  Credentials(Credentials&&);
  Credentials& operator=(Credentials&&);
  Credentials(const Credentials&) = delete;
  Credentials& operator=(const Credentials&) = delete;

  grpc_channel_credentials* get_credentials() const { return creds_; }

 private:
  grpc_channel_credentials* creds_;
};

}  // namespace client
}  // namespace easy_grpc

#endif