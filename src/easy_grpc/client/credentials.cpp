// Copyright 2019 Age of Minds inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "easy_grpc/client/credentials.h"
#include <string>

namespace easy_grpc {
namespace client {

Credentials::Credentials() : creds_(nullptr) {}

Credentials::Credentials(const char* root_certs, const char* private_key, const char* trust_chain) {
  grpc_ssl_pem_key_cert_pair key_cert;
  grpc_ssl_pem_key_cert_pair* key_cert_ptr;
  if (private_key != nullptr || trust_chain != nullptr) {
    key_cert.private_key = private_key;
    key_cert.cert_chain = trust_chain;
    key_cert_ptr = &key_cert;
  } else {
    key_cert_ptr = nullptr;
  }

  creds_ = grpc_ssl_credentials_create_ex(root_certs, key_cert_ptr, nullptr, nullptr);
}

Credentials::~Credentials() {
  if (creds_ != nullptr) {
    grpc_channel_credentials_release(creds_);
  }
}

Credentials::Credentials(Credentials&& rhs) {
  creds_ = rhs.creds_;
  rhs.creds_ = nullptr;
}

Credentials& Credentials::operator=(Credentials&& rhs) {
  if (creds_ != nullptr) {
    grpc_channel_credentials_release(creds_);
  }
  creds_ = rhs.creds_;
  rhs.creds_ = nullptr;
  return *this;
}

}  // namespace client
}  // namespace easy_grpc